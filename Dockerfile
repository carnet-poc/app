FROM node:10-jessie

# Install default dependencies
RUN apt-get update && apt-get install -y curl \
	git

COPY . /opt/app
WORKDIR /opt/app
